FROM debian:bullseye-backports

ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y; apt-get full-upgrade -y; apt-get install -y \
	wget \
	curl \
	git \
	dmidecode \
	ca-certificates \
	build-essential \
	rsync \
	python3-jinja2 \
	debian-archive-keyring \
	gnupg

RUN curl -s https://packagecloud.io/install/repositories/varnishcache/varnish70/script.deb.sh | bash
RUN apt-get update -y; apt-get install -y varnish varnish-dev; apt-get clean; rm -rf /var/lib/apt/lists/*

RUN wget https://github.com/varnish/varnish-modules/releases/download/0.19.0/varnish-modules-0.19.0.tar.gz ; tar xvzf varnish-modules-0.19.0.tar.gz ; cd varnish-modules-0.19.0 ; ./configure ; make ; make check ; make install

EXPOSE 8080
CMD /usr/sbin/varnishd -F -a http=:8080,HTTP -f /etc/varnish/default.vcl -s malloc,512m
